import { Message } from "./message";
import { Result } from "./result";
import { Severity } from "./severity";

/* force colors on when running stylish tests */
const defaultColor = process.env.FORCE_COLOR;
process.env.FORCE_COLOR = "1";

import { stylish } from "./stylish";

/* restore color, need only to be set when importing library */
process.env.FORCE_COLOR = defaultColor;

function createResults(...messages: Message[]): Result[] {
	const errorCount = messages.filter((it) => it.severity === Severity.ERROR).length;
	const warningCount = messages.filter((it) => it.severity === 1).length;
	return [
		{
			filePath: "path/to/mock-file.js",
			errorCount,
			warningCount,
			fixableErrorCount: 0,
			fixableWarningCount: 0,
			messages,
		},
	];
}

it("should format results", () => {
	expect.assertions(1);
	const results = createResults(
		{
			ruleId: "my-rule",
			severity: Severity.ERROR,
			message: "a fixable error",
			line: 47,
			column: 11,
		},
		{
			ruleId: "another-rule",
			severity: Severity.WARN,
			message: "warning message",
			line: 49,
			column: 3,
		},
		{
			ruleId: "third-rule",
			severity: Severity.ERROR,
			message: "this error cannot be fixed automatically",
			line: 49,
			column: 6,
		},
	);
	results[0].fixableWarningCount = 1;
	expect(stylish(results)).toMatchInlineSnapshot(`
		"</>
		<underline>path/to/mock-file.js</underline>
		  <dim>47:11</intensity>  <red>error</color>    a fixable error                           <dim>my-rule</intensity>
		  <dim>49:3</intensity>   <yellow>warning</color>  warning message                           <dim>another-rule</intensity>
		  <dim>49:6</intensity>   <red>error</color>    this error cannot be fixed automatically  <dim>third-rule</intensity>

		<red><bold>✖ 3 problems (2 errors, 1 warning)
		</intensity></color><red><bold>  0 errors and 1 warning potentially fixable with the \`--fix\` option.
		</intensity></color></>"
	`);
});

it("should return empty string when results are empty", () => {
	expect.assertions(1);
	expect(stylish([])).toBe("");
});

it("should return empty string when messages are empty", () => {
	expect.assertions(1);
	const results = createResults();
	expect(stylish(results)).toBe("");
});

it("should color error messages red", () => {
	expect.assertions(1);
	const results = createResults({
		ruleId: "mock-rule",
		severity: Severity.ERROR,
		message: "error message",
		line: 1,
		column: 1,
	});
	expect(stylish(results)).toMatchInlineSnapshot(`
		"</>
		<underline>path/to/mock-file.js</underline>
		  <dim>1:1</intensity>  <red>error</color>  error message  <dim>mock-rule</intensity>

		<red><bold>✖ 1 problem (1 error, 0 warnings)
		</intensity></color></>"
	`);
});

it("should color fatal messages red", () => {
	expect.assertions(1);
	const results = createResults({
		ruleId: "mock-rule",
		severity: Severity.WARN,
		message: "warning message",
		fatal: true,
		line: 1,
		column: 1,
	});
	expect(stylish(results)).toMatchInlineSnapshot(`
		"</>
		<underline>path/to/mock-file.js</underline>
		  <dim>1:1</intensity>  <red>error</color>  warning message  <dim>mock-rule</intensity>

		<red><bold>✖ 1 problem (0 errors, 1 warning)
		</intensity></color></>"
	`);
});

it("should color warning messages yellow", () => {
	expect.assertions(1);
	const results = createResults({
		ruleId: "mock-rule",
		severity: Severity.WARN,
		message: "warning message",
		line: 1,
		column: 1,
	});
	expect(stylish(results)).toMatchInlineSnapshot(`
		"</>
		<underline>path/to/mock-file.js</underline>
		  <dim>1:1</intensity>  <yellow>warning</color>  warning message  <dim>mock-rule</intensity>

		<yellow><bold>✖ 1 problem (0 errors, 1 warning)
		</intensity></color></>"
	`);
});

it("should use line and column if present", () => {
	expect.assertions(1);
	const results = createResults({
		ruleId: "mock-rule",
		severity: Severity.ERROR,
		message: "error message",
		line: 1,
		column: 2,
	});
	expect(stylish(results)).toContain("1:2");
});

it("should center line:column at colon", () => {
	expect.assertions(1);
	const results = createResults(
		{
			ruleId: "mock-rule",
			severity: Severity.ERROR,
			message: "error message",
			line: 1,
			column: 2,
		},
		{
			ruleId: "mock-rule",
			severity: Severity.ERROR,
			message: "error message",
			line: 11,
			column: 22,
		},
	);
	expect(stylish(results)).toMatchInlineSnapshot(`
		"</>
		<underline>path/to/mock-file.js</underline>
		   <dim>1:2</intensity>   <red>error</color>  error message  <dim>mock-rule</intensity>
		  <dim>11:22</intensity>  <red>error</color>  error message  <dim>mock-rule</intensity>

		<red><bold>✖ 2 problems (2 errors, 0 warnings)
		</intensity></color></>"
	`);
});

it("should handle missing line and column", () => {
	expect.assertions(1);
	const results = createResults({
		ruleId: "mock-rule",
		severity: Severity.ERROR,
		message: "error message",
	});
	expect(stylish(results)).toContain("0:0");
});

it("should handle missing ruleId", () => {
	expect.assertions(1);
	const results = createResults({
		severity: Severity.ERROR,
		message: "error message",
		line: 1,
		column: 2,
	});
	expect(stylish(results)).toMatchInlineSnapshot(`
		"</>
		<underline>path/to/mock-file.js</underline>
		  <dim>1:2</intensity>  <red>error</color>  error message

		<red><bold>✖ 1 problem (1 error, 0 warnings)
		</intensity></color></>"
	`);
});

it("should sum errors and warnings", () => {
	expect.assertions(1);
	const results = createResults(
		{
			ruleId: "mock-rule",
			severity: Severity.ERROR,
			message: "error message",
			line: 1,
			column: 1,
		},
		{
			ruleId: "mock-rule",
			severity: Severity.WARN,
			message: "warning message",
			line: 1,
			column: 1,
		},
		{
			ruleId: "mock-rule",
			severity: Severity.ERROR,
			message: "error message",
			line: 1,
			column: 1,
		},
	);
	expect(stylish(results)).toContain("3 problems (2 errors, 1 warning)");
});

it("should pluralize", () => {
	expect.assertions(5);
	const error1 = createResults({
		ruleId: "mock-rule",
		severity: Severity.ERROR,
		message: "error message",
	});
	const error2 = createResults({
		ruleId: "mock-rule",
		severity: Severity.ERROR,
		message: "error message",
	});
	const warning1 = createResults({
		ruleId: "mock-rule",
		severity: Severity.WARN,
		message: "warning message",
	});
	const warning2 = createResults({
		ruleId: "mock-rule",
		severity: Severity.WARN,
		message: "warning message",
	});
	expect(stylish(error1)).toContain("1 problem (1 error, 0 warnings)");
	expect(stylish(warning1)).toContain("1 problem (0 errors, 1 warning)");
	expect(stylish([...error1, ...error2])).toContain("2 problems (2 errors, 0 warnings)");
	expect(stylish([...warning1, ...warning2])).toContain("2 problems (0 errors, 2 warnings)");
	expect(stylish([...error1, ...warning1])).toContain("2 problems (1 error, 1 warning)");
});

it("should show summary of fixable errors", () => {
	expect.assertions(1);
	const results = createResults({
		ruleId: "mock-rule",
		severity: Severity.ERROR,
		message: "error message",
	});
	results[0].fixableErrorCount = 1;
	expect(stylish(results)).toContain("1 error and 0 warnings potentially fixable");
});

it("should show multiple files", () => {
	expect.assertions(1);
	const results1 = createResults({
		ruleId: "mock-rule",
		severity: Severity.ERROR,
		message: "error message",
	});
	const results2 = createResults({
		ruleId: "mock-rule",
		severity: Severity.ERROR,
		message: "error message",
	});
	const results = [...results1, ...results2];
	results[0].filePath = "first.js";
	results[1].filePath = "second.js";
	expect(stylish(results)).toMatchInlineSnapshot(`
		"</>
		<underline>first.js</underline>
		  <dim>0:0</intensity>  <red>error</color>  error message  <dim>mock-rule</intensity>

		<underline>second.js</underline>
		  <dim>0:0</intensity>  <red>error</color>  error message  <dim>mock-rule</intensity>

		<red><bold>✖ 2 problems (2 errors, 0 warnings)
		</intensity></color></>"
	`);
});
