function passtru(value: string): string {
	return value;
}

export const reset = passtru;
export const bold = passtru;
export const dim = passtru;
export const underline = passtru;

export const yellow = passtru;
export const red = passtru;
