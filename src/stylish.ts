import table, { type Options } from "text-table";
import * as colors from "./colors-node";
import { type Result } from "./result";
import { Severity } from "./severity";

/**
 * Given a word and a count, append an s if count is not one.
 * @param word - A word in its singular form.
 * @param count - A number controlling whether word should be pluralized.
 * @returns - The original word with an s on the end if count is not one.
 */
function pluralize(word: string, count: number): string {
	return count === 1 ? word : `${word}s`;
}

/**
 * Given a line with " $\{line\} $\{column\} " where line is right-aligned and
 * column is left-aligned it replaces the delimiter with a colon.
 */
function centerLineColumn(el: string): string {
	/* eslint-disable-next-line sonarjs/slow-regex -- false positive? unless I'm missing something here */
	return el.replace(/(\d+)\s+(\d+)/u, (_m, p1: string, p2: string) => colors.dim(`${p1}:${p2}`));
}

/**
 * Strip ANSI color escape sequences from text.
 */
function stripAnsi(text: string): string {
	/* eslint-disable-next-line no-control-regex -- expected to match control characters */
	return text.replace(/\u001B\[[0-9;]*m/g, "");
}

export function stylish(results: Result[]): string {
	let output = "\n";
	let errorCount = 0;
	let warningCount = 0;
	let fixableErrorCount = 0;
	let fixableWarningCount = 0;
	let summaryColor: colors.Colorize = colors.yellow;

	results.forEach((result) => {
		const messages = result.messages;

		if (messages.length === 0) {
			return;
		}

		errorCount += result.errorCount;
		warningCount += result.warningCount;
		fixableErrorCount += result.fixableErrorCount;
		fixableWarningCount += result.fixableWarningCount;

		const rows = messages.map((message) => {
			let messageType;

			if (Boolean(message.fatal) || message.severity === Severity.ERROR) {
				messageType = colors.red("error");
				summaryColor = colors.red;
			} else {
				messageType = colors.yellow("warning");
			}

			return [
				"",
				message.line ?? 0,
				message.column ?? 0,
				messageType,
				message.message.replace(/([^ ])\.$/u, "$1"),
				message.ruleId ? colors.dim(message.ruleId) : "",
			];
		});

		const options: Options = {
			align: ["l", "r", "l"],
			stringLength(str: string): number {
				return stripAnsi(str).length;
			},
		};
		const formattedTable = table(rows, options).split("\n").map(centerLineColumn).join("\n");

		output += `${colors.underline(result.filePath)}\n`;
		output += `${formattedTable}\n\n`;
	});

	const total = errorCount + warningCount;

	if (total > 0) {
		output += summaryColor(
			colors.bold(
				[
					"\u2716 ",
					total,
					pluralize(" problem", total),
					" (",
					errorCount,
					pluralize(" error", errorCount),
					", ",
					warningCount,
					pluralize(" warning", warningCount),
					")\n",
				].join(""),
			),
		);

		if (fixableErrorCount > 0 || fixableWarningCount > 0) {
			output += summaryColor(
				colors.bold(
					[
						"  ",
						fixableErrorCount,
						pluralize(" error", fixableErrorCount),
						" and ",
						fixableWarningCount,
						pluralize(" warning", fixableWarningCount),
						" potentially fixable with the `--fix` option.\n",
					].join(""),
				),
			);
		}
	}

	// Resets output color, for prevent change on top level
	return total > 0 ? colors.reset(output) : "";
}
