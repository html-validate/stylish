import { type Severity } from "./severity";

export interface Message {
	ruleId?: string;
	severity: Severity;
	fatal?: boolean;
	message: string;
	line?: number;
	column?: number;
}
