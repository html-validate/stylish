import * as kleur from "kleur/colors";

export type Colorize = kleur.Colorize;

export const reset = kleur.reset;
export const bold = kleur.bold;
export const dim = kleur.dim;
export const underline = kleur.underline;

export const yellow = kleur.yellow;
export const red = kleur.red;
