# ESLint compatible stylish formatter

Standalone, API compatible and refactored version of the popular stylish formatter from ESLint.
