/* eslint-disable no-console -- expected to log */
import fs from "node:fs/promises";
import path from "node:path";
import * as esbuild from "esbuild";

const formatType = {
	esm: "module",
	cjs: "commonjs",
};

function colorsStub({ enabled }) {
	return {
		name: "colors-stub",
		setup(build) {
			if (!enabled) {
				return;
			}
			build.onResolve({ filter: /colors-node/ }, (args) => {
				return { path: path.join(args.resolveDir, "colors-browser.ts") };
			});
		},
	};
}

async function run() {
	for (const format of ["cjs", "esm"]) {
		for (const flavour of ["browser", "node"]) {
			const result = await esbuild.build({
				entryPoints: ["src/index.ts"],
				outfile: `dist/${format}/${flavour}.js`,
				sourcemap: true,
				bundle: true,
				platform: "node",
				target: "node14",
				format,
				external: ["kleur"],
				metafile: true,
				logLevel: "info",
				plugins: [colorsStub({ enabled: flavour === "browser" })],
			});
			if (format === "esm") {
				console.log(await esbuild.analyzeMetafile(result.metafile));
			}
			const pkg = { type: formatType[format] };
			await fs.writeFile(`dist/${format}/package.json`, JSON.stringify(pkg, null, 2), "utf-8");
		}
	}
}

run().catch((err) => {
	console.error(err);
	process.exitCode = 1;
});
