# @html-validate/stylish changelog

## [4.2.0](https://gitlab.com/html-validate/stylish/compare/v4.1.0...v4.2.0) (2023-07-30)

### Features

- **deps:** require nodejs 16 or later ([6b5d531](https://gitlab.com/html-validate/stylish/commit/6b5d5319a8b92c97b076e6391ac9d0927b96ae7c))

## [4.1.0](https://gitlab.com/html-validate/stylish/compare/v4.0.1...v4.1.0) (2023-06-10)

### Features

- browser build ([af90379](https://gitlab.com/html-validate/stylish/commit/af903791f01a5d99fedf5ecbe0a307f646f83876))

## [4.0.1](https://gitlab.com/html-validate/stylish/compare/v4.0.0...v4.0.1) (2023-05-03)

### Bug Fixes

- esm build fix ([58fd13c](https://gitlab.com/html-validate/stylish/commit/58fd13c8bb4b0fb6ce2d71d1bd443c14969d307c))

## [4.0.0](https://gitlab.com/html-validate/stylish/compare/v3.0.1...v4.0.0) (2023-04-14)

### ⚠ BREAKING CHANGES

- this package now only provides named exports

```diff
-import stylish from "@html-validate/stylish"
+import { stylish } from "@html-validate/stylish"
```

### Features

- esm/cjs hybrid ([185c3ba](https://gitlab.com/html-validate/stylish/commit/185c3ba7cf399500a719f918464f04e054a7cf8c))
- named exports only ([7121f29](https://gitlab.com/html-validate/stylish/commit/7121f29519bee5c4db28202feedd3480fc58aa69))

### [3.0.1](https://gitlab.com/html-validate/stylish/compare/v3.0.0...v3.0.1) (2022-05-25)

### Bug Fixes

- expose `Severity` enum ([78f18a7](https://gitlab.com/html-validate/stylish/commit/78f18a7739084995d878d63b796bd18d8058cd77))

## [3.0.0](https://gitlab.com/html-validate/stylish/compare/v2.0.1...v3.0.0) (2022-05-15)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- bundle `text-table` dependency ([981c96e](https://gitlab.com/html-validate/stylish/commit/981c96e5e34e5c36d85010cc472dd40e25fdc531))
- require node 14 ([5b6320c](https://gitlab.com/html-validate/stylish/commit/5b6320c463aa21411cdc701991c79abb13b449ba))

### Dependency upgrades

- **deps:** pin dependency text-table to 0.2.0 ([50c2ea1](https://gitlab.com/html-validate/stylish/commit/50c2ea10be653a2a102f89cbebd546189d9a2b82))

### [2.0.1](https://gitlab.com/html-validate/stylish/compare/v2.0.0...v2.0.1) (2021-07-25)

### Bug Fixes

- set `sideEffects` to false ([21f4a79](https://gitlab.com/html-validate/stylish/commit/21f4a79e8e722332785af3e8d6935deb17ca0679))

## [2.0.0](https://gitlab.com/html-validate/stylish/compare/v1.0.1...v2.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([f99ade9](https://gitlab.com/html-validate/stylish/commit/f99ade9c2c395f3faca0ec75e92e94b3fe3448cb))

### [1.0.1](https://gitlab.com/html-validate/stylish/compare/v1.0.0...v1.0.1) (2021-04-27)

### Dependency upgrades

- **deps:** replace `chalk` with `kleur` ([6d80b65](https://gitlab.com/html-validate/stylish/commit/6d80b65226ca1fbc4ddf24e035e37f8aac3b4cea))

## 1.0.0 (2020-11-15)

### Features

- fork and migrate to typescript with testcases ([652f4be](https://gitlab.com/html-validate/stylish/commit/652f4be99c539a3653f659c40d821bf39e8aad9b))

### Bug Fixes

- default export ([a2fa549](https://gitlab.com/html-validate/stylish/commit/a2fa5493ee5efd772213140d43d7e2f795887e50))
